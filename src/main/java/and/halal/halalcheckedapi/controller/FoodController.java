package and.halal.halalcheckedapi.controller;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import and.halal.halalcheckedapi.exceptions.ProductNotFoundException;
import and.halal.halalcheckedapi.exceptions.ServiceIndisponibleException;
import and.halal.halalcheckedapi.utilil.Additif;
import and.halal.halalcheckedapi.utilil.Additifs;
import and.halal.halalcheckedapi.utilil.Produit;
import and.halal.halalcheckedapi.utilil.Reponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * Classe gérant les requéte entrant 
 * 
 * @author elhadjimalicK
 *
 */
@Api(description = "Vérification du caractére halal d'un produit.")
@RestController
public class FoodController {
	
	/*
	 *  3095758097010 : pour tester un produit halal
	 *  3242272500056 : pour tester un produit non halal
	 *  3103220030462 : pour tester un produit douteux
	 */

	//String[] haramsAdditifs = {"e120","e441","e519","e542",};	
	//String[] douteuxAdditifs= {"E101","E101i","E101ii","E101iii","E101a","E140","E141","E141i","E141ii","E160f","E161a","E161b","E161bi","E161bii","E161c","E161d","E161e","E161f","E161g","E161h","E161hi","E161hii","E162","E163","E163ii","E163iii","E163iv","E163v","E170","E170i","E170ii","E304","E322","E322i","E322ii","E334","E335","E335i","E335ii","E336","E336i","E336ii","E337","E353","E354","E387","E431","E432","E433","E434","E435","E436","E462","E470","E470i","E470ii","E471","E472a","E472b","E472c","E472d","E472e","E472f","E472g","E473","E473a","E474","E475","E476","E477","E478","E479","E481","E481i","E481ii","E482","E482i","E482ii","E483","E484","E491","E492","E493","E494","E495","E570","E572","E620","E627","E630","E631","E632","E633","E634","E635","E640","E900","E900a","E900b","E904","E912","E92"};
	/**
	 * methode traitant les requêtes de type GET vers l'url food/{id}
	 * @param le code bare du produit
	 * @return	une chaine contenant la liste des ingrédients du produit
	 * @throws IOException
	 * @throws ProductNotFoundException 
	 * @throws ServiceIndisponibleException 
	 */
	@ApiOperation(value = "vérifie si un produit est halal.")
	@GetMapping(value = "checkIsHalal/{id}")
    public Reponse checkProduct(@PathVariable long id, HttpServletRequest request) throws IOException, ProductNotFoundException, ServiceIndisponibleException {
		String openFactReponse, imargeUrl,  haram_ingredients = "", additive_id = "", additive_name = "", additive_description = "";
		int repStatut = 0;
		boolean halal = true, doute = false;
		Additif additif,additif2;
		
	StringBuffer s =request.getRequestURL();
        
		URL url = new URL("https://world.openfoodfacts.org/api/v0/product/" + id + ".json");
		
        openFactReponse = LaunchQuery.sendRequestGetHttps(url);
        
        if (openFactReponse == null) {
        	throw new ServiceIndisponibleException("Le service est temporairement indisponible!!! Merci de réessayer ultérieurement");
        	/*repStatut = -4;
        	halal =false;
        	doute =false;
        	return new Reponse(id, repStatut, halal, doute, null);*/
        }
        
        JsonReader jsonReader = Json.createReader(new StringReader(openFactReponse));

        JsonObject object = jsonReader.readObject();
        
        int status = object.getInt("status");
        
        // vérifie si le produit est halal ou pas quand le produit existe dans la base de données d'open fact food
        if (status == 1){
       
        	String p1 = request.getParameter("p1") , p2 = request.getParameter("p2");
        	if (p1 == null)
    			p1 = "";
    		if (p2 == null)
    			p2 = "" ;
        	
        	additif = Additifs.haram(openFactReponse,p1,p2);
        	if (!additif.isHalal()) {
        		halal =false;
        		repStatut = -2;
        		if ((additif.getStr()[0] != null) && (additif.getStr()[1] == null)) {
        			haram_ingredients = additif.getStr()[0];
        			additif.getStr()[0] = "";
        		}	
        		else {
        			additive_id = additif.getStr()[0];
        			additive_name = additif.getStr()[1];
        			additive_description = additif.getStr()[2];
        		}
        	}
        	
        	
        	additif2 = Additifs.douteux(openFactReponse, p1, p2);
        	if (additif.isHalal())
        		if (additif2.isDoute()) {
        			doute = true;
            		repStatut = -1;
            		additive_id = additif2.getStr()[0];
            		additive_name = additif2.getStr()[1];
            		additive_description = additif2.getStr()[2];
        		}	
        	} else {
        		throw new ProductNotFoundException("le produit recherché n'existe pas encore dans notre base de données");
        		/*repStatut = -3;
        		halal =false;
            	doute =false;
            	return new Reponse(id, repStatut, halal, doute, null);*/
        	}
        
        JsonReader jsonReader2 = Json.createReader(new StringReader(openFactReponse));
        object = jsonReader2.readObject().getJsonObject("product");

        imargeUrl = object.getString("image_front_url");
        if (imargeUrl == null)
        	imargeUrl = "https://cdn.pixabay.com/photo/2016/11/02/08/56/binary-1790842_960_720.jpg";
       
        Produit produit = new Produit(object.getString("countries"),object.getString("ingredients_text_fr"), haram_ingredients, additive_id, additive_name , additive_description, imargeUrl);
        Reponse reponse = new Reponse(id, repStatut, halal, doute, produit);
        
        return reponse ;
        }
}
