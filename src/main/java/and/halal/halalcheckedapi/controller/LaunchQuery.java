package and.halal.halalcheckedapi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Classe d'utilitaire pour la gestion des rêquéte vers l'API d'open food facts 
 * 
 * @author elhadjimalicK
 *
 */
public class LaunchQuery {
	
	/**
	 * Exécute une commande dans le répeetoir spécifié
	 * @param la commande à éxécuter
	 * @param le répertoire ou la commade sera éxécutée
	 * @return le message issu de l'éxécution de la commande 
	 * @throws IOException si la lecture du résultat de texte a rencontré un problème
	 */
	public static String executeCommand(String command, File dir) throws IOException{
		StringBuilder output = new StringBuilder();
		String line;

		Process process = Runtime.getRuntime().exec(command, null, dir);
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

		while ((line = bufReader.readLine()) != null){
			output.append(line+System.getProperty("line.separator"));
		}
		return output.toString()+"et"+bufReader.toString();
	}
	
	/**
	 * 
	 * @param un flux d'entrée de données
	 * @return la chaine correspondant au flux d'entrée
	 * @throws IOException
	 */
	public static String readStream(InputStream is) throws IOException {
	    StringBuilder sb = new StringBuilder();  
	    BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);  
	    for (String line = r.readLine(); line != null; line =r.readLine()){  
	        sb.append(line);  
	    }  
	    is.close();  
	    return sb.toString();
	}

	
	/**
	 * Envoi une requette GET à l'url passé en paramétre
	 * @param l'url traitant la requette
	 * @return la chaine représentant le résultat de la requette
	 * @throws IOException
	 */
	public static String sendRequestGetHttps(URL url) throws  IOException{
		String response = null;

		HttpsURLConnection connection = (HttpsURLConnection)url.openConnection(); 
		
		connection.setRequestMethod("GET");

		// le code réponse est 200 OK 
		if (String.valueOf(connection.getResponseCode()).startsWith("2"))
			response = readStream(connection.getInputStream());

		return response;
	}
}
