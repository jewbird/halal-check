package and.halal.halalcheckedapi.utilil;

/**
 * Un objet de cette classe est retourné par les méthodes testant le caractére halal des produits
 * La classe conttient uniquement un constructeur, des getters et setters
 * @author elhadjimalickthiam
 *
 */
public class Additif {

	/**
	 * Vrai si l'additif est halal
	 */
	private boolean halal;
	/**
	 * vrai si l'additif est douteux
	 */
	private boolean doute;
	/**
	 * tableau de chaines de caratéres contenant le code de l'additif son nom et sa description 
	 */
	private String[] str;
	
	public Additif(boolean halal, boolean doute, String[] str) {
		super();
		this.halal = halal;
		this.doute = doute;
		this.str = str;
	}

	public boolean isHalal() {
		return halal;
	}

	public void setHalal(boolean halal) {
		this.halal = halal;
	}

	public boolean isDoute() {
		return doute;
	}

	public void setDoute(boolean doute) {
		this.doute = doute;
	}

	public String[] getStr() {
		return str;
	}

	public void setStr(String[] str) {
		this.str = str;
	}
	
	
		
}
