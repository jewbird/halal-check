package and.halal.halalcheckedapi.utilil;

public class Reponse {
	private long code;
	private int query_status;
	private boolean isHalal;
	private boolean isDoubful;
	private Produit product_details;
	
	public Reponse() {
		
	}

	public Reponse(long code, int query_status, boolean isHalal, boolean isDoubful, Produit product_details) {
		super();
		this.code = code;
		this.query_status = query_status;
		this.isHalal = isHalal;
		this.isDoubful = isDoubful;
		this.product_details = product_details;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public int getQuery_status() {
		return query_status;
	}

	public void setQuery_status(int query_status) {
		this.query_status = query_status;
	}

	public boolean isHalal() {
		return isHalal;
	}

	public void setHalal(boolean isHalal) {
		this.isHalal = isHalal;
	}

	public boolean isDoubful() {
		return isDoubful;
	}

	public void setDoubful(boolean isDoubful) {
		this.isDoubful = isDoubful;
	}

	public Produit getProduct_details() {
		return product_details;
	}

	public void setProduct_details(Produit product_details) {
		this.product_details = product_details;
	}

	
}
